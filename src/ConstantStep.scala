/**
  * Created by arsenii on 20.12.16.
  */
class ConstantStep[T,X,Y](initialStep:T, integrator: Integrator[T,X,Y]) extends Step[T,X,Y](integrator){

  def getInitialStep(end:T) = initialStep

  def apply(origin:(X,Y),step:T):(T,(X,Y)) = {
    def res = integrator.integrationStep(origin)(step)
    (step,res)
  }

}
