/**
  * Created by arsenii on 21.12.16.
  */
class HeuristicStep(delta:BigDecimal=BigDecimal("1e-12"))(integrator: Integrator[BigDecimal,BigDecimal,BigDecimal])
  extends Step[BigDecimal,BigDecimal,BigDecimal](integrator){

  override def getInitialStep(end:BigDecimal) = end

  protected def apply(origin:(BigDecimal,BigDecimal),step: BigDecimal):(BigDecimal,(BigDecimal,BigDecimal))={

    val res0 = integrator.integrationStep(origin)(step)

    def recurse(fullstep:(BigDecimal,BigDecimal),origin:(BigDecimal,BigDecimal),step:BigDecimal):(BigDecimal,BigDecimal)={
      val halfstep = step/BigDecimal("2.0")
      val left = integrator.integrationStep(origin)(halfstep)
      val right = integrator.integrationStep(left)(halfstep)
      if((fullstep._2-right._2).abs<delta)
        right
      else{
        def left2 = recurse(left,origin,halfstep)
        recurse(right,left2,step/BigDecimal("2.0"))
      }
    }
    def result = recurse(res0,origin,step)
    (step,result)
  }


}

