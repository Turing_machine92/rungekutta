/**
  * Created by arsenii on 20.12.16.
  */


abstract class Integrator[T,X,Y](derivative:(X,Y)=>Y)(origin:(X,Y)) {

  def getOrigin = origin

  def integrationStep(origin:(X,Y))(step:T):(X,Y)

  def vectorStep(origin:(Seq[(X,Y)]))(step:T):(Seq[(X,Y)])={
     origin.par.map(integrationStep(_)(step)).seq
  }

}

