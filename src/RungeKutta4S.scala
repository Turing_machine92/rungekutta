/**
  * Created by arsenii on 21.12.16.
  */
class RungeKutta4S (derivative: (BigDecimal,BigDecimal)=>BigDecimal)(origin:(BigDecimal,BigDecimal))
extends Integrator[BigDecimal,BigDecimal,BigDecimal](derivative)(origin) {


  def integrationStep(origin: (BigDecimal, BigDecimal))(step: BigDecimal): (BigDecimal, BigDecimal) = {
    def k(k0:BigDecimal,step: BigDecimal) = derivative(origin._1+step,origin._2+k0*step)
    val k1 = derivative(origin._1,origin._2)
    val k2 = k(k1,step/BigDecimal("2.0"))
    val k3 = k(k2,step/BigDecimal("2.0"))
    val k4 = k(k3,step)
    val sum = (step/BigDecimal("6.0"))*(k1+2*k2+2*k3+k4)
    (origin._1+step,origin._2+sum)
  }



}
