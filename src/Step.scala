import scala.math.ScalaNumber

/**
  * Created by arsenii on 20.12.16.
  */


abstract class Step[T,X,Y](integrator: Integrator[T,X,Y]){

  def getInitialStep(end:T):T

  def integrate()(endMoment:T):Y ={

    def call(origin:(T,(X,Y))):Y = {
      val newOrigin = apply(origin._2,origin._1)
      if(newOrigin._1==endMoment)
        newOrigin._2._2
      else
        call(newOrigin)
    }
    val origin =integrator.getOrigin
    call(getInitialStep(endMoment),origin)
  }



  protected def apply(origin:(X,Y),step: T):(T,(X,Y))
}
