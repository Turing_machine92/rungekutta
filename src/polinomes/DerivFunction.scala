package polinomes

/**
  * Created by arsenii on 29.12.16.
  */
abstract class DerivFunction[T,R] extends Function1[T,R]{

  def derivative(v: T): R
}
