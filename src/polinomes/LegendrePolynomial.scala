package polinomes

import solving.NewtonMethod

import scala.collection.parallel.ParSeq

/**
  * Created by arsenii on 29.12.16.
  */
class LegendrePolynomial(power: Int) extends  DerivFunction[BigDecimal,BigDecimal]{

  override def derivative(x: BigDecimal): BigDecimal = {
    (power/(1-x.pow(2)))*(apply(x,power-1)-x*apply(x))
  }

  def apply(x:BigDecimal,power:Int):BigDecimal ={

    def apply(x:BigDecimal,cur:Int,power:Int,value:BigDecimal,prevValue:BigDecimal):BigDecimal = {
      if(cur==power)
        value
      else {
        apply(x, cur + 1, power, ((2*cur+ 1) / (cur + 1)) * x * value - (cur / (cur + 1)) * prevValue, value)
      }
    }
    if(power==0)
      BigDecimal("1")
    else if(power==1)
      x
    else {
      apply(x,1,power,x,BigDecimal("1"))
    }
  }

  override def apply(v: BigDecimal): BigDecimal ={
    apply(v,power)
  }


  lazy val startingRootAproximation : ParSeq[Double] = {
    (1 until power+1) map (i=>Math.cos(Math.PI*(4*i-1)/(4*power+2))) par
  }

  lazy val rootsOfPolynomial : ParSeq[BigDecimal] = {
    val nm = new NewtonMethod(this,1e-12)
    startingRootAproximation map (nm(_))
  }

}


