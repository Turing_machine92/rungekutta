package solving

import polinomes.DerivFunction

/**
  * Created by arsenii on 29.12.16.
  */
class NewtonMethod(function: DerivFunction[BigDecimal,BigDecimal], accuracy:BigDecimal) {

  def apply(x0:BigDecimal):BigDecimal = {
    val x1 = x0 - function(x0) / function.derivative(x0)
    if ((x1 - x0).abs < accuracy) x1
    else apply(x1)
  }

}
